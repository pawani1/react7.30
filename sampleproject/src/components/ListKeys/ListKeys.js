import React from "react";
import template from "./ListKeys.jsx";

class ListKeys extends React.Component {
  render() {
    return template.call(this);
  }
}

export default ListKeys;
