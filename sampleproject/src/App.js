import {Component} from "react";
import ChildToParent from "./components/ChildToParent/ChildToParent";
import ListKeys from "./components/ListKeys/ListKeys";
import ListKeys1 from "./components/ListKeys/LIstKeys1";
import ListKeys2 from "./components/ListKeys/ListKeys2";
import {ListKeys3} from "./components/ListKeys/ListKeys3";
import ConditionalRendering from "./components/ConditionalRendering";
import ConditionalRendering1 from "./components/ContionalRendering1";
import ConditionalRendering2 from "./components/ConditionalRendering2";
import ConditionalRendering3 from "./components/ConditionalRendering3";
import EventBindingInReact from "./components/EventBindingInReact";
import ContextInReact from "./components/ContextInReact";
import ContextInReact1 from "./components/ContextInReact1";
import SetStateArgs from "./components/SetStateArgs";
import Ajax from "./components/Ajax";
import TakingTheData1 from "./components/takingTheRef2";
import TakingTheREf3 from "./components/TakingTheREf3";
import WithoutJSX from "./components/WithoutJSX";
import WithoutJSX1 from "./components/WithoutJSX1";
import GetSum from "./ArithmeticFunctionalComp";
import TakeDataByOnchangeFunctional from "./components/TakeDataByOnchangeFunctional";
import TakeDataByOnchangeFunctionalForIn from "./components/TakeByOnchangeFunctionalForIn";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Menu from "./components/Menu";

class App extends Component {
  constructor() {
    super();
    console.log("constructor executed");
    this.state = {
      name: "ST"
    };
  }
  callBack = (d) => {
    this.setState({
      name: d
    });
  };
  render() {
    console.log("render executed");
    return (
      <div>
        {/* <h1>I'm App:{this.state.name}</h1>
       
        <ListKeys />
        <ListKeys1></ListKeys1>
        <ListKeys2 />
        <ListKeys3></ListKeys3> */}
        {/* <EventBindingInReact /> */}
        {/* <ContextInReact /> */}
        {/* <ContextInReact1 /> */}
        {/* <TakingTheData1 /> */}
        {/* <TakingTheREf3 /> */}
        {/* <WithoutJSX1 /> */}
        {/* <GetSum /> */}
        {/* <TakeDataByOnchangeFunctionalForIn /> */}
        <Header />
        <Menu />
        {/* <ChildToParent getNameFromChild={this.callBack} location="mumbai">
          Hyderabad
        </ChildToParent> */}
        {/* <h1>{this.state.name}</h1> */}
        <Footer />
      </div>
    );
  }
}
export default App;
